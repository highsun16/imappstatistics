__author__ = 'Highsun'

from DB.DBHandler import *

kBufferSize = 10000

class BaseDAO(object):

    dbHandler = DBHandler()
    buffer = []

    @classmethod
    def insertObject(cls, obj):
        if obj is None:
            return
        with cls.dbHandler.db().cursor() as cursor:
            fieldStr = cls.__fieldStringForObject(cls, obj)
            sql = "INSERT IGNORE INTO %s %s" % (cls.tableName(), fieldStr)
            cursor.execute(sql)
            last_id = cursor.lastrowid
        cls.dbHandler.db().commit()
        return last_id

    @classmethod
    def insertObjectIntoBuffer(cls, obj):
        fieldStr = cls.__fieldStringForObject(cls, obj)
        sql = "INSERT IGNORE INTO %s %s" % (cls.tableName(), fieldStr)
        if len(cls.buffer) == kBufferSize:
            with cls.dbHandler.db().cursor() as cursor:
                for sqlInBuffer in cls.buffer:
                    cursor.execute(sqlInBuffer)
            cls.dbHandler.db().commit()
            cls.buffer.clear()
        else:
            cls.buffer.append(sql)

    @classmethod
    def cleanBuffer(cls):
        if len(cls.buffer) == 0:
            return
        with cls.dbHandler.db().cursor() as cursor:
            for sqlInBuffer in cls.buffer:
                cursor.execute(sqlInBuffer)
        cls.dbHandler.db().commit()
        cls.buffer.clear()

    @staticmethod
    def tableName():
        pass

    @staticmethod
    def fields():
        pass

    @staticmethod
    def __fieldStringForObject(cls, obj):
        keyStr = ""
        valueStr = ""
        for key in cls.fields():
            if obj.get(key) is None:
                continue

            keyStr += "%s," % key
            if isinstance(obj.get(key), str):
                valueStr += "\"%s\"," % obj.get(key)
            else:
                valueStr += "%s," % obj.get(key)
        fieldStr = "(%s) VALUES (%s)" % (keyStr[:-1], valueStr[:-1])
        return fieldStr
