__author__ = 'Highsun'

from DB.BaseDAO import *


class ConvCreateDAO(BaseDAO):

    @staticmethod
    def tableName():
        return "conv_create"

    @staticmethod
    def fields():
        return {"timestamp", "duration", "network", "login_id"}
