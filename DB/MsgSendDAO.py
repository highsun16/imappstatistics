__author__ = 'Highsun'

from DB.BaseDAO import *


class MsgSendDAO(BaseDAO):

    @staticmethod
    def tableName():
        return "msg_send"

    @staticmethod
    def fields():
        return ["timestamp", "duration", "network", "login_id"]
