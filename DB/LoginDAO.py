__author__ = 'Highsun'

from DB.BaseDAO import *


class LoginDAO(BaseDAO):

    @staticmethod
    def tableName():
        return "login"

    @staticmethod
    def fields():
        return ["app_name", "app_version", "device", "sys_ver", "ucid", "timestamp", "rom"]
