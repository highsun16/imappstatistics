__author__ = 'Highsun'
from DB.BaseDAO import *


class MsgSyncDAO(BaseDAO):

    @staticmethod
    def tableName():
        return "msg_sync"

    @staticmethod
    def fields():
        return ["timestamp", "duration", "network", "login_id"]
