__author__ = 'Highsun'

import pymysql


class DBHandler(object):
    def __init__(self):
        self._db = pymysql.connect(
            host='localhost',
            db='im_statistics',
            user='root',
            passwd='',
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor
        )

    def db(self):
        return self._db
