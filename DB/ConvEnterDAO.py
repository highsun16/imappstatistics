__author__ = 'Highsun'

from DB.BaseDAO import *


class ConvEnterDAO(BaseDAO):

    @staticmethod
    def tableName():
        return "conv_enter"

    @staticmethod
    def fields():
        return ["timestamp", "duration", "network", "login_id"]
