__author__ = 'Highsun'

from DB.BaseDAO import *


class MsgSyncTotalDAO(BaseDAO):

    @staticmethod
    def tableName():
        return "msg_sync_total"

    @staticmethod
    def fields():
        return ["timestamp", "duration", "network", "login_id", "msg_count"]
