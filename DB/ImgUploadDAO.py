__author__ = 'Highsun'

from DB.BaseDAO import *


class ImgUploadDAO(BaseDAO):

    @staticmethod
    def tableName():
        return "media_upload"

    @staticmethod
    def fields():
        return ["timestamp", "duration", "network", "login_id", "file_size"]
