# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.26)
# Database: im_statistics
# Generation Time: 2016-11-24 13:10:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table conv_create
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conv_create`;

CREATE TABLE `conv_create` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(13) NOT NULL,
  `duration` double NOT NULL,
  `network` varchar(10) DEFAULT NULL,
  `login_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table conv_enter
# ------------------------------------------------------------

DROP TABLE IF EXISTS `conv_enter`;

CREATE TABLE `conv_enter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(13) NOT NULL,
  `duration` double NOT NULL,
  `network` varchar(10) DEFAULT NULL,
  `login_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table login
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `app_name` varchar(20) NOT NULL DEFAULT '',
  `app_version` varchar(20) NOT NULL DEFAULT '',
  `device` varchar(30) NOT NULL DEFAULT '',
  `sys_ver` varchar(20) NOT NULL DEFAULT '',
  `ucid` varchar(20) NOT NULL DEFAULT '',
  `timestamp` bigint(13) NOT NULL,
  `rom` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ucid` (`ucid`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table media_upload
# ------------------------------------------------------------

DROP TABLE IF EXISTS `media_upload`;

CREATE TABLE `media_upload` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(13) NOT NULL,
  `duration` double NOT NULL,
  `network` varchar(10) DEFAULT NULL,
  `login_id` int(11) NOT NULL,
  `file_size` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table msg_send
# ------------------------------------------------------------

DROP TABLE IF EXISTS `msg_send`;

CREATE TABLE `msg_send` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(13) NOT NULL,
  `duration` double NOT NULL,
  `network` varchar(10) DEFAULT NULL,
  `login_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table msg_sync
# ------------------------------------------------------------

DROP TABLE IF EXISTS `msg_sync`;

CREATE TABLE `msg_sync` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(13) NOT NULL,
  `duration` double NOT NULL,
  `network` varchar(10) DEFAULT NULL,
  `login_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table msg_sync_total
# ------------------------------------------------------------

DROP TABLE IF EXISTS `msg_sync_total`;

CREATE TABLE `msg_sync_total` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` bigint(13) NOT NULL,
  `duration` double NOT NULL,
  `network` varchar(10) DEFAULT NULL,
  `login_id` int(11) NOT NULL,
  `msg_count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
