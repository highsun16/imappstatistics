__author__ = 'Highsun'
import json
import os
import shutil
import zipfile
import Utils
from DB.BaseDAO import *
from DB.LoginDAO import *
from DB.ConvEnterDAO import *
from DB.ConvCreateDAO import *
from DB.ImgUploadDAO import *
from DB.MsgSendDAO import *
from DB.MsgSyncDAO import *
from DB.MsgSyncTotalDAO import *
import config
import logging
import time

logging.basicConfig(filename='error.log', level=logging.DEBUG)

class LogReader:

    @classmethod
    def writeLogsIntoDB(cls):
        if not os.path.exists(config.tmpFilePath):
            os.makedirs(config.tmpFilePath)
        for root, subdirs, files in os.walk(config.logPath):
            for filename in files:
                if filename.endswith(".zip"):
                    if os.path.getsize(os.path.join(root, filename)) == 0:
                        continue
                    else:
                        # avoid bad zip file
                        try:
                            cls.unzipFile(os.path.join(root, filename))
                        except Exception as e:
                            logging.error('%s   Exception: %s, file: %s' % (time.asctime(time.localtime(time.time())), e, os.path.join(root, filename)))
                            continue
                else:
                    cls.writeLogIntoDB(os.path.join(root, filename))
        cls.writeUnzippedLogIntoDB()
        cls.cleanDAOBuffer()
        # clear tmp
        shutil.rmtree(config.tmpFilePath)
        # move logs to archieve
        for subdir in os.listdir(config.logPath):
            if subdir.endswith(".DS_Store"):
                continue
            shutil.move(os.path.join(config.logPath, subdir), config.archievedLogPath)


    @classmethod
    def writeLogIntoDB(cls, logPath):
        filename = os.path.basename(logPath)
        if filename.endswith(".DS_Store"):
            return
        with open(logPath) as fp:
            last_login_id = 0
            # 预防文件解码失败
            # 一堆 try catch,代码没法看了啊
            try:
                for line in fp:
                    # 预防某行有错误的 JSON 字符串
                    try:
                        line_obj = json.loads(line)
                    except Exception as e:
                        logging.error('%s   Exception: %s, file: %s, line: %s' % (time.asctime(time.localtime(time.time())), e, logPath, line))
                        continue
                    line_type = line_obj.get("type")
                    ucid = line_obj.get("ucid")

                    if ucid is not None:    #login 类型
                        # 旧版本缺失字段补齐
                        cls.compatibleWithOldLog(line_obj, filename)

                        last_login_id = LoginDAO.insertObject(line_obj)
                    else:
                        # ignore invalid last_login_id
                        if last_login_id == 0:
                            continue
                        line_obj["login_id"] = last_login_id
                        cls.compatibleWithAndroidLog(line_obj)
                        DAO = globals()[Utils.DAOClassNameFromType(line_type)]
                        DAO.insertObjectIntoBuffer(line_obj)
            except Exception as e:
                logging.error('%s   Exception: %s, file: %s' % (time.asctime(time.localtime(time.time())), e, logPath))
                pass

    @classmethod
    def unzipFile(cls, file):
        with zipfile.ZipFile(file, "r") as zip_ref:
            zip_ref.extractall("%s/%s" % (config.tmpFilePath, file))

    @classmethod
    def writeUnzippedLogIntoDB(cls):
        for root, subdirs, files in os.walk(config.tmpFilePath):
            for filename in files:
                cls.writeLogIntoDB(os.path.join(root, filename))

    @classmethod
    def cleanDAOBuffer(cls):
        BaseDAO.cleanBuffer()

    #兼容旧 Log
    @classmethod
    def compatibleWithOldLog(cls, line_obj, filename):
        if line_obj.get('timestamp') is None:
            line_obj["timestamp"] = Utils.timestampFromLogFile(os.path.splitext(filename)[0])
        if line_obj.get('sys_ver') is None:
            line_obj["sys_ver"] = Utils.systemVersionFromLogFile(os.path.splitext(filename)[0])
        if line_obj.get('app_name') is None:
            line_obj['app_name'] = Utils.appNameFromLogFile(os.path.splitext(filename)[0])
        if line_obj.get('rom') is None:
            line_obj['rom'] = ''

    @classmethod
    def compatibleWithAndroidLog(cls, line_obj):
        # 安卓缺少 duration 的情况
        if line_obj.get('duration') is None:
            line_obj["duration"] = (line_obj.get('endTime') - line_obj.get('startTime')) / 1000
        # 安卓旧版本 log 上传图片没有 file_size 字段, 暂时手动设置为 0
        if line_obj.get('type') == 'img_upload':
            if line_obj.get('file_size') is None:
                line_obj["file_size"] = 0
