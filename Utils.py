__author__ = 'Highsun'

import time
import datetime


def timestampFromLogFile(filename):
    timestampFromLogFile.timestampOffset += 1
    timestampFromLogFile.timestampOffset %= 1000
    time_component = filename.split('_')[-1]
    return time.mktime(datetime.datetime.strptime(time_component, "%Y%m%d").timetuple()) * 1000 + timestampFromLogFile.timestampOffset
timestampFromLogFile.timestampOffset = 0

def systemVersionFromLogFile(filename):
    origin_sys_ver = filename.split('_')[0]
    return "%s %s" % (origin_sys_ver[:3], origin_sys_ver[3:])

def appNameFromLogFile(filename):
    app_name = filename.split('_')[1]
    return app_name

def DAOClassNameFromType(type):
    type_component = type.split('_')
    class_name = ""
    for component in type_component:
        class_name += component.capitalize()
    class_name += "DAO"
    return class_name
